﻿using System;
using System.Collections.Generic;
using UnityEngine;

public static class GameMarketManager
{

    private static int _refreshTimeInMinutes = 1;

    public static Action OnUpdateAction;
    public static GameMarket CurrentMarket
    {
        get
        {
            if (_currentMarket == null)
                _currentMarket = GetMarket();

            return _currentMarket;
        }
    }
    private static GameMarket _currentMarket;

    private static void RefreshCurrentMarket()
    {
        _currentMarket = CreateNewMarket(JsonMarketSaveLoadUtil.GetDefaulGetConfigurationFromJson());
        JsonMarketSaveLoadUtil.SaveMarketState(_currentMarket, true);
        OnUpdateAction?.Invoke();
    }

    private static GameMarket GetMarket()
    {
        var currentState = JsonMarketSaveLoadUtil.LoadMarketState();
        if (DateTime.Now.Ticks > currentState.SaveTime + TimeSpan.TicksPerMinute * _refreshTimeInMinutes)
        {
            currentState = CreateNewMarket(JsonMarketSaveLoadUtil.GetDefaulGetConfigurationFromJson());
            JsonMarketSaveLoadUtil.SaveMarketState(currentState, true);
        }
        return currentState;
    }

    public static void BuyItemFromMarket(GameMarket.MarketItem marketItem)
    {
        marketItem.BoughtCounter++;
        JsonMarketSaveLoadUtil.SaveMarketState(CurrentMarket, false);
    }

    public static bool CanBuyItemFromMarket(GameMarket.MarketItem marketItem)
    {
        return marketItem.MaxCounter > marketItem.BoughtCounter;
    }

    private static GameMarket CreateNewMarket(MarketConfiguration fullMarketConfig)
    {
        var newMarketRoll = new MarketConfiguration();
        newMarketRoll.PossibleMarketSetups = MarketRoll.GetRolledMarketSetups(fullMarketConfig.PossibleMarketSetups);
        
        var newGameMarket = new GameMarket()
        {
            MarketSetups = new List<GameMarket.MarketSetup>(),
        };
        
        foreach (var marketSetup in newMarketRoll.PossibleMarketSetups)
        {
            var newMarketSetup = new GameMarket.MarketSetup
            {
                Id = marketSetup.Id,
                PossibleBlocks = new List<GameMarket.MarketBlock>()
            };
            newGameMarket.MarketSetups.Add(newMarketSetup);
            
            marketSetup.PossibleBlocks = MarketRoll.GetRolledMarketBlocksByRolledSet(marketSetup);
            foreach (var block in marketSetup.PossibleBlocks)
            {
                var newMarketBlock = new GameMarket.MarketBlock
                {
                    Id = block.Id,
                    PossibleItems = new List<GameMarket.MarketItem>()
                };
                newMarketSetup.PossibleBlocks.Add(newMarketBlock);
                block.PossibleItems = MarketRoll.GetRolledMarketItemsByMarketBlock(block);
                foreach (var possibleItem in block.PossibleItems)
                {
                    var rolledItem =new GameMarket.MarketItem
                    {
                        Id = possibleItem.Id,
                        MaxCounter = possibleItem.MaxCounter,
                        BoughtCounter = 0
                    };
                    newMarketBlock.PossibleItems.Add(rolledItem);
                }
            }
        }

        return newGameMarket;

    }

    public static int GetRefreshTimeLeftInSeconds()
    {
        var refreshTime = CurrentMarket.SaveTime + TimeSpan.TicksPerMinute * _refreshTimeInMinutes;
        var secondsLeft = (refreshTime - DateTime.Now.Ticks) / TimeSpan.TicksPerSecond;
        if (secondsLeft < 0)
        {
            Debug.Log("Update market");
            RefreshCurrentMarket();
        }

        return (int)secondsLeft;
    }


    public class GameMarket
    {
        public List<MarketSetup> MarketSetups;
        public long SaveTime;

        [Serializable]
        public class MarketItem
        {
            public string Id;
            public int MaxCounter;
            public int BoughtCounter;
        }
        
        [Serializable]
        public class MarketBlock
        {
            public string Id;
            public List<MarketItem> PossibleItems;
        }
        
        [Serializable]
        public class MarketSetup
        {
            public string Id;
            public List<MarketBlock> PossibleBlocks;

        }
    }
}
