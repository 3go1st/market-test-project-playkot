﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MarketConfiguration
{
    public List<MarketSetup> PossibleMarketSetups = new List<MarketSetup>();

    [Serializable]
    public class MarketItem : WeightRollItem
    {
        public int MaxCounter;
    }

    [Serializable]
    public class MarketBlock : WeightRollItem
    {
        public int PossibleItemsCounter;
        public List<MarketItem> PossibleItems;
    }
    
    [Serializable]
    public class MarketSetup : WeightRollItem
    {
        public int PossibleBlocksCounter;
        public List<MarketBlock> PossibleBlocks;
    }
}
