﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

public static class MarketRoll
{
    public static List<MarketConfiguration.MarketSetup> GetRolledMarketSetups(List<MarketConfiguration.MarketSetup> allSets)
    {
        var rollDictionary = new Dictionary<string, float>();
        foreach (var set in allSets)
        {
            rollDictionary.Add(set.Id, set.Weight);
        }

        var rolledIds = DiceRoll.GetRolledIds(rollDictionary, 1);
        var resultList = new List<MarketConfiguration.MarketSetup>();
        foreach (var id in rolledIds)
        {
            resultList.Add(allSets.First(x=> x.Id == id));
        }

        return resultList;
    }

    public static List<MarketConfiguration.MarketBlock> GetRolledMarketBlocksByRolledSet(MarketConfiguration.MarketSetup rolledSetup)
    {
        var rollDictionary = new Dictionary<string, float>();
        foreach (var block in rolledSetup.PossibleBlocks)
        {
            rollDictionary.Add(block.Id, block.Weight);
        }

        var rolledIds = DiceRoll.GetRolledIds(rollDictionary, rolledSetup.PossibleBlocksCounter);
        var resultList = new List<MarketConfiguration.MarketBlock>();
        foreach (var id in rolledIds)
        {
            resultList.Add(rolledSetup.PossibleBlocks.First(x=> x.Id == id));
        }

        return resultList;
    }

    public static List<MarketConfiguration.MarketItem> GetRolledMarketItemsByMarketBlock(MarketConfiguration.MarketBlock marketBlock)
    {
        var rollDictionary = new Dictionary<string, float>();
        foreach (var block in marketBlock.PossibleItems)
        {
            rollDictionary.Add(block.Id, block.Weight);
        }

        var rolledIds = DiceRoll.GetRolledIds(rollDictionary, marketBlock.PossibleItemsCounter);
        var resultList = new List<MarketConfiguration.MarketItem>();
        foreach (var id in rolledIds)
        {
            resultList.Add(marketBlock.PossibleItems.First(x=> x.Id == id));
        }

        return resultList;
    }

}
