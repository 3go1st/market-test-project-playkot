﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "MarketScriptableObject", menuName = "Create Market Configuration", order = 1)]
public class MarketScriptableObj: ScriptableObject
{
    public List<MarketSetup> PossibleMarketSets = new List<MarketSetup>();

    [Serializable]
    public class MarketItem : WeightRollItem
    {
        public int MaxCounter;
    }

    [Serializable]
    public class MarketBlock : WeightRollItem
    {
        public int PossibleItemsCounter;
        public List<MarketItem> PossibleItems;
    }
    
    [Serializable]
    public class MarketSetup : WeightRollItem
    {
        public int PossibleBlocksCounter;
        public List<MarketBlock> PossibleBlocks;
    }
}
