﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class MarketCanvasUI : MonoBehaviour
{
    [SerializeField] private ScrollRect _marketSetupsScrollView;
    [SerializeField] private ScrollRect _marketBlocksScrollView;
    [SerializeField] private ScrollRect _marketItemsScrollView;
    
    [SerializeField] private GameObject _contentElementButton;
    private GameMarketManager.GameMarket _marketModel;

    [SerializeField] private TextMeshProUGUI _timerText;
    private List<MarketScrollContentButton> _buttonsPool = new List<MarketScrollContentButton>();//sort of 

    private void Start()
    {
        MakeMarketCanvas();
    }

    private void OnEnable()
    {
        GameMarketManager.OnUpdateAction += MakeMarketCanvas;
    }

    private void OnDisable()
    {
        GameMarketManager.OnUpdateAction -= MakeMarketCanvas;
    }

    private void MakeMarketCanvas()
    {
        SetCurrentGameMarketState();
        RefreshCanvases();
        RefreshGameMarketElements();
    }

    private void Update()
    {
        var timeLeft = GameMarketManager.GetRefreshTimeLeftInSeconds();
        var minutesLeft = timeLeft / 60;
        var secondsLeft = timeLeft % 60;
        _timerText.text = $"Refresh in : {minutesLeft:D2}:{secondsLeft:D2}";
    }

    private void SetCurrentGameMarketState()
    {
        _marketModel = GameMarketManager.CurrentMarket;
    }


    private void RefreshCanvases()
    {
        _marketSetupsScrollView.gameObject.SetActive(true);
        ClearOldButtons(_marketSetupsScrollView.content);
        
        _marketBlocksScrollView.gameObject.SetActive(false);
        ClearOldButtons(_marketBlocksScrollView.content);
        
        _marketItemsScrollView.gameObject.SetActive(false);
        ClearOldButtons(_marketItemsScrollView.content);
    }
    
    private void ClearOldButtons(RectTransform rectTransform)
    {
        for (int i = 0; i < rectTransform.transform.childCount; i++)
        {
            rectTransform.GetChild(i).gameObject.SetActive(false);
        }
    }

    private void RefreshGameMarketElements()
    {
        foreach (var marketSetup in _marketModel.MarketSetups)
        {
            var marketSetButton = GetButtonFromPool(_marketSetupsScrollView.content);
            marketSetButton.SetButton(()=>
            {
                RefreshMarketBlocks(marketSetup);
                _marketBlocksScrollView.gameObject.SetActive(true);
            }, marketSetup.Id);
            
        }
    }

    private void RefreshMarketBlocks(GameMarketManager.GameMarket.MarketSetup chosenMarketSetup)
    {
        ClearOldButtons(_marketBlocksScrollView.content);
        foreach (var block in chosenMarketSetup.PossibleBlocks)
        {
            var blockButton = GetButtonFromPool(_marketBlocksScrollView.content);
            blockButton.SetButton(()=>
            {
                RefreshMarketItems(block);
                _marketItemsScrollView.gameObject.SetActive(true);
            }, block.Id);
        }
    }

    private void RefreshMarketItems(GameMarketManager.GameMarket.MarketBlock chosenMarketBlock)
    {
        ClearOldButtons(_marketItemsScrollView.content);
        foreach (var item in chosenMarketBlock.PossibleItems)
        {
            if(!GameMarketManager.CanBuyItemFromMarket(item))
                continue;

            var itemButton = GetButtonFromPool(_marketItemsScrollView.content);
            itemButton.SetButton(() =>
            {
                if (GameMarketManager.CanBuyItemFromMarket(item))
                {
                    GameMarketManager.BuyItemFromMarket(item);
                    RefreshMarketItems(chosenMarketBlock);
                }
            }, item.Id);
        }
    }

    private MarketScrollContentButton GetButtonFromPool(RectTransform neededRectTransform)
    {
        if (_buttonsPool.Exists(x => !x.gameObject.activeSelf))
        {
            var button = _buttonsPool.First(x => !x.gameObject.activeSelf);
            button.transform.SetParent(neededRectTransform);
            button.gameObject.SetActive(true);
            return button;
        }
        else
        {
            var button = Instantiate(_contentElementButton, neededRectTransform, false);
            var marketScrollContentButton = button.GetComponent<MarketScrollContentButton>();
            _buttonsPool.Add(marketScrollContentButton);
            return marketScrollContentButton;
        }
    }

}
