﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class MarketScrollContentButton : MonoBehaviour
{
    private TextMeshProUGUI _buttonText;
    private Button _button;
    public string ButtonName = "default";

    public void SetButton(UnityAction onClickAction, string buttonName)
    {
        ButtonName = buttonName;
        _buttonText = transform.GetComponentInChildren<TextMeshProUGUI>();
        _button = GetComponent<Button>();
        _buttonText.text = buttonName;
        _button.onClick.RemoveAllListeners();
        _button.onClick.AddListener(onClickAction);
    }
}
