﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class DiceRoll 
{
    public class RollItem
    {
        public string Id;
        public float Weight;
        public float DropPercent;
    }

    public static List<string> GetRolledIds(Dictionary<string, float> itemsToRoll, int resultItemsCounter)
    {
        var rolledItems = new List<string>();
        for (int i = 0; i < resultItemsCounter; i++)
        {
            var totalWeight = 0f;
            var tempItemsToRoll = MakeRollItemsList(itemsToRoll);

            var rollValue = Random.Range(0, 1f);
            var totalRoll = 0f;
            foreach (var tempRollitem in tempItemsToRoll)
            {
                totalRoll += tempRollitem.DropPercent;
                if (totalRoll > rollValue && !rolledItems.Exists(x => x == tempRollitem.Id))
                {
                    rolledItems.Add(tempRollitem.Id);
                    itemsToRoll.Remove(tempRollitem.Id);
                    break;
                }
            }
        }


        return rolledItems;
    }
    
    


    private static List<RollItem> MakeRollItemsList(Dictionary<string,float> possibleItems)
    {
        var newRollList = new List<RollItem>();
        foreach (var item in possibleItems)
        {
            newRollList.Add(new RollItem()
            {
                Id = item.Key,
                Weight = item.Value,
                DropPercent = 0
            });
        }

        var totalWeight = 0f;
        foreach (var item in possibleItems)
        {
            totalWeight += item.Value;
        }
        
        foreach (var rollItem in newRollList)
        {
            rollItem.DropPercent = rollItem.Weight / totalWeight;
        }

        return newRollList;
    }
}
