﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

public class JsonMarketSaveLoadUtil : MonoBehaviour
{
    private static string _fileName = $"jsonMarketModel";
    private static string _savedFileName = "CurrentSavedMarket";
    private static string _pathToFolder = $"{Application.dataPath}/Jsons";

    [SerializeField] private MarketConfiguration _marketConfiguration;
    
    //if you want to make new model from scriptableObject, assign script at any object on any scene,
    //set _marketConfiguration(scriptablObject) and call create json from context menu
    [ContextMenu("Create Json")]
    private void CreateJson()
    {
        var jsonData = JsonUtility.ToJson(_marketConfiguration, true);
        Debug.Log(jsonData);
        WriteToFile(jsonData, _fileName);
    }
    public static void SaveMarketState(GameMarketManager.GameMarket currentMarket,bool updateTimer)
    {
        if(updateTimer)
            currentMarket.SaveTime = DateTime.Now.Ticks;
        
        var jsonData = JsonUtility.ToJson(currentMarket, true);
        WriteToFile(jsonData, _savedFileName);
    }

    public static GameMarketManager.GameMarket LoadMarketState()
    {
        if (!File.Exists($"{Application.persistentDataPath}/{_savedFileName}"))
        {
            return new GameMarketManager.GameMarket();
        }

        var fileText = File.ReadAllText($"{Application.persistentDataPath}/{_savedFileName}");
        var marketConfiguration = JsonUtility.FromJson<GameMarketManager.GameMarket>(fileText);
        return marketConfiguration;
    }

    private static void WriteToFile(string jsonData, string fileName)
    {
        var writeStream = File.CreateText($"{_pathToFolder}/{fileName}");
        writeStream.Write(jsonData);
        writeStream.Close();
    }

    public static MarketConfiguration GetDefaulGetConfigurationFromJson()
    {
        var pathToFile = $"{_pathToFolder}/{_fileName}";

        var fileText = File.ReadAllText(pathToFile);
        var marketConfiguration = JsonUtility.FromJson<MarketConfiguration>(fileText);
        return marketConfiguration;
    }
}
