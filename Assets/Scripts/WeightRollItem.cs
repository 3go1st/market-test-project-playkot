﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeightRollItem
{
    public string Id;
    public float Weight;
}
